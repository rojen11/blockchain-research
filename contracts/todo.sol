pragma solidity ^0.5.16;


contract todo {
    bool public image = true;
    string ipfsHash = "QmbJKenRot2bPdTr3vHiYHQdTWwEgPZa3rjKkUFLpyLLsX";

    event ImageAdded(string ipfsHash);

    function setImage(string memory _hash) public returns (string memory) {
        image = true;
        ipfsHash = _hash;
        emit ImageAdded(_hash);
        return "test";
    }

    function getImage() public view returns (string memory) {
        require(image == true, "No image available.");
        return ipfsHash;
    }
}
