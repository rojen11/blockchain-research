<?php
	require_once('./vendor/autoload.php');
	use Web3\Contract;

	include_once('web.php');
	include_once('get_image.php');

	// $contract->at($)

	$imgsrc = null;

	if ($imagedata != null){
		$imgsrc = $src;
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>BlockChain</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="#">Blockchain</a>
	</nav>
	<div class="container mt-5">
		<center>
			<?php if ($imgsrc != null) {
				echo "<img src=\"$imgsrc\" width=\"500\">";
			}else{
				echo "no image found";
			}
			?>
		</center>
		<br>
		<center>
			<form action="/image_upload.php" method="post" enctype="multipart/form-data">
				<input class="btn-primary" type="file" name="file" id="file">
				<br>
				<input class="btn-primary mt-2" type="submit" value="Submit">
			</form>
		</center>
	</div>
</body>
</html>