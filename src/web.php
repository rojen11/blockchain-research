<?php
	require_once('./vendor/autoload.php');
	use Web3\Contract;

	use Web3\Web3;

	
	class Web{
		private $todojson = "/home/aron/Work/TodoApp/build/contracts/todo.json";
		private $data;
		private $address;
		private $con;
		private $transactionId;
		private $conAddr;
		
		function __construct(){
			$openfile = fopen($this->todojson,"r");
			$file = fread($openfile,filesize($this->todojson));
			$this->data = json_decode($file, true);

			$this->con = new Contract('http://localhost:8545', $this->getabi());

			$this->con->bytecode($this->getBytecode())->new([
				'from' => '0x87ed3d314570A23E77FBdD71F7c2A4671F38d5b2',
				'gas' => '0x200b20'
			],function ($err, $result) {
				if ($err !== null) {
					throw $err;
				}
				$this->transactionId = $result;

				$this->con->eth->getTransactionReceipt($result, function ($err, $transaction){
					if ($err !== null) {
						throw $err;
					}
					if ($transaction) {
						$this->conAddr= $transaction->contractAddress;
					}
				});
			});
		}
		
		public function getabi(){
			
			return $this->data["abi"];
		}
		
		// public function getAddress(){
		// 	return $this->data["networks"]["1586843013281"]["address"];
		// }
		
		public function getBytecode(){
			return $this->data["bytecode"];
		}
		
		public function setContractAddress($addr){
			$this->address = $addr;
		}

		public function getContractAddress(){
			return $this->conAddr;
		}

		public function getTransactionId(){
			return $this->transactionId;
		}

		public function getContract(){
			return $this->con;
		}
	}
	
	$web3 = new Web3('http://localhost:8545');
	$web = new Web();
	$contract = $web->getContract();
