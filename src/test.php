<?php

	include_once("ipfs.php");
	include_once("web.php");
	
	$bytecode = $web->getBytecode();
	echo "hello <br>";

	$web3->eth->accounts(function ($err, $accounts) use ($contract, $bytecode) {
		if ($err === null) {
			if (isset($accounts)) {
				$accounts = $accounts;
			} else {
				throw new RuntimeException('Please ensure you have access to web3 json rpc provider.');
			}
			$fromAccount = $accounts[0];
			$toAccount = $accounts[1];
			$contract->bytecode($bytecode)->new([
				'from' => $fromAccount,
				'gas' => '0x200b20'
			], function ($err, $result) use ($contract, $fromAccount, $toAccount) {
				echo "<br>";
				if ($err !== null) {
					throw $err;
				}
				echo "<br>";
				if ($result) {
					echo "\nTransaction has made:) id: " . $result . "\n";
				}
				$transactionId = $result;

				$contract->eth->getTransactionReceipt($transactionId, function ($err, $transaction) use ($contract, $fromAccount, $toAccount) {
					echo "<br>";	
					if ($err !== null) {
						throw $err;
					}
					if ($transaction) {
						$contractAddress = $transaction->contractAddress;
						echo "\nTransaction has mind:) block number: " . $transaction->blockNumber . "\n";
						echo "<br>";
						$contract->at($contractAddress)->call('setImage', "hello", [
							'from' => $fromAccount,
							'gas' => '0x200b20'
						], function ($err, $result) use ($contract, $contractAddress, $toAccount) {
							if ($err !== null) {
								throw $err;
							}
							var_dump($result);
							$contract->at($contractAddress)->call('getImage', function($err,$result){
								if ($err !== null){
									throw $err;
								}
								var_dump($result);
							});
							// if ($result) {
							// 	echo "\nTransaction has made:) id: " . $result . "\n";
							// }
							echo "<br>";
							// $contract->eth->getTransactionReceipt($transactionId, function ($err, $transaction) use ($fromAccount, $toAccount) {
							// 	if ($err !== null) {
							// 		throw $err;
							// 	}
							// 	if ($transaction) {
							// 		echo "\nTransaction has mind:) block number: " . $transaction->blockNumber . "\nTransaction dump:\n";
							// 		var_dump($transaction);
							// 	}
							// });
						});
					}
				});
			});
		}
	});