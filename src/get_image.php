<?php

	include_once("ipfs.php");
	include_once("web.php");

	$hash = null;

	$contract->at($web->getContractAddress())->call("getImage", function( $err, $result) use (& $hash){
		if ($err !== null){
			// echo "No image available";
			echo $err;
		}
		if ($result){ 
			$hash = $result[0];
		}
	});

	$imagedata = explode(" ", $ipfs->cat($hash))[0];


	// Format the image SRC:  data:{mime};base64,{data};
	$src = 'data: image/png;base64,'.$imagedata;

